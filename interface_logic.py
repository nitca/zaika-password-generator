"""Interface logic of programm"""
"""All functions and logic for use"""


from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtGui import QIntValidator
from passgen_interface import Ui_MainWindow
from algorithm import genpass_random, genpass_custom


class PassgenWindow(QMainWindow):
    """Main window class"""
    def __init__(self, app):
        super(PassgenWindow, self).__init__()

        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.app = app

        self.from_number = int(self.ui.from_text.text())
        self.to_number = int(self.ui.from_text.text())

        pass_string = genpass_random(self.from_number, self.to_number)
        self.ui.pass_label.setText(pass_string)
        self.ui.random_radio.setChecked(True)

        self.ui.random_radio.clicked.connect(self.user_choise)
        self.ui.custom_radio.clicked.connect(self.user_choise)

        self.ui.from_text.setValidator(QIntValidator(1, 64))
        self.ui.to_text.setValidator(QIntValidator(1, 64))
        self.ui.numbers_text.setValidator(QIntValidator(1, 64))
        self.ui.letters_text.setValidator(QIntValidator(1, 64))
        self.ui.symbols_text.setValidator(QIntValidator(1, 64))
        self.ui.upletters_text.setValidator(QIntValidator(1, 64))

        self.ui.from_text.editingFinished.connect(self.user_choise)
        self.ui.to_text.editingFinished.connect(self.user_choise)
        self.ui.numbers_text.editingFinished.connect(self.user_choise)
        self.ui.letters_text.editingFinished.connect(self.user_choise)
        self.ui.symbols_text.editingFinished.connect(self.user_choise)
        self.ui.upletters_text.editingFinished.connect(self.user_choise)

    def user_choise(self):
        """One function for generate different password by radio buttons"""
        if self.ui.random_radio.isChecked():
            self.ui.custom_box.setEnabled(False)
            self.ui.random_box.setEnabled(True)
            pass_string = genpass_random(self.from_number, self.to_number)
            self.ui.pass_label.setText(pass_string)

            if self.app.clipboard() != None:
                self.app.clipboard().setText(pass_string)

        if self.ui.custom_radio.isChecked():
            self.ui.random_box.setEnabled(False)
            self.ui.custom_box.setEnabled(True)

            numbers = int(self.ui.numbers_text.text())
            letters = int(self.ui.letters_text.text())
            symbols = int(self.ui.symbols_text.text())
            upletters = int(self.ui.upletters_text.text())

            pass_string = genpass_custom(numbers, letters, symbols, upletters)

            self.ui.pass_label.setText(pass_string)

            if self.app.clipboard() != None:
                self.app.clipboard().setText(pass_string)
