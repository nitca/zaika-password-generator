#!/usr/bin/env python
"""Main file of Zaika - Password Generator"""

import sys
from interface_logic import PassgenWindow
from PyQt5.QtWidgets import QApplication


def main():
    app = QApplication([])
    passgen_window = PassgenWindow(app)

    passgen_window.show()

    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
