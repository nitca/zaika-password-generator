# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'passgen_interface.ui'
#
# Created by: PyQt5 UI code generator 5.15.0
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(497, 335)
        MainWindow.setMinimumSize(QtCore.QSize(497, 335))
        MainWindow.setMaximumSize(QtCore.QSize(497, 335))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("zaika.ico"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        MainWindow.setWindowIcon(icon)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.centralwidget.setObjectName("centralwidget")
        self.random_box = QtWidgets.QGroupBox(self.centralwidget)
        self.random_box.setEnabled(True)
        self.random_box.setGeometry(QtCore.QRect(20, 30, 210, 230))
        self.random_box.setTitle("")
        self.random_box.setObjectName("random_box")
        self.gridLayoutWidget = QtWidgets.QWidget(self.random_box)
        self.gridLayoutWidget.setGeometry(QtCore.QRect(10, 20, 61, 201))
        self.gridLayoutWidget.setObjectName("gridLayoutWidget")
        self.gridLayout = QtWidgets.QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.from_label = QtWidgets.QLabel(self.gridLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.from_label.setFont(font)
        self.from_label.setObjectName("from_label")
        self.gridLayout.addWidget(self.from_label, 1, 0, 1, 1)
        self.to_label = QtWidgets.QLabel(self.gridLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.to_label.setFont(font)
        self.to_label.setObjectName("to_label")
        self.gridLayout.addWidget(self.to_label, 2, 0, 1, 1)
        self.from_text = QtWidgets.QLineEdit(self.random_box)
        self.from_text.setGeometry(QtCore.QRect(70, 56, 61, 29))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.from_text.setFont(font)
        self.from_text.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.from_text.setStyleSheet("border: solid;\n"
"border-width: 1px;\n"
"border-color: rgb(0, 0, 0);")
        self.from_text.setInputMethodHints(QtCore.Qt.ImhDigitsOnly|QtCore.Qt.ImhFormattedNumbersOnly|QtCore.Qt.ImhPreferNumbers)
        self.from_text.setInputMask("")
        self.from_text.setMaxLength(32767)
        self.from_text.setCursorPosition(2)
        self.from_text.setObjectName("from_text")
        self.to_text = QtWidgets.QLineEdit(self.random_box)
        self.to_text.setGeometry(QtCore.QRect(70, 159, 61, 29))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.to_text.setFont(font)
        self.to_text.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.to_text.setStyleSheet("border: solid;\n"
"border-width: 1px;\n"
"border-color: rgb(0, 0, 0);")
        self.to_text.setInputMethodHints(QtCore.Qt.ImhDigitsOnly|QtCore.Qt.ImhFormattedNumbersOnly|QtCore.Qt.ImhPreferNumbers)
        self.to_text.setInputMask("")
        self.to_text.setMaxLength(32767)
        self.to_text.setCursorPosition(2)
        self.to_text.setObjectName("to_text")
        self.custom_box = QtWidgets.QGroupBox(self.centralwidget)
        self.custom_box.setEnabled(False)
        self.custom_box.setGeometry(QtCore.QRect(270, 30, 210, 230))
        self.custom_box.setTitle("")
        self.custom_box.setObjectName("custom_box")
        self.gridLayoutWidget_2 = QtWidgets.QWidget(self.custom_box)
        self.gridLayoutWidget_2.setGeometry(QtCore.QRect(10, 20, 86, 201))
        self.gridLayoutWidget_2.setObjectName("gridLayoutWidget_2")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.gridLayoutWidget_2)
        self.gridLayout_2.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.letters_label = QtWidgets.QLabel(self.gridLayoutWidget_2)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.letters_label.setFont(font)
        self.letters_label.setObjectName("letters_label")
        self.gridLayout_2.addWidget(self.letters_label, 1, 0, 1, 1)
        self.symbols_label = QtWidgets.QLabel(self.gridLayoutWidget_2)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.symbols_label.setFont(font)
        self.symbols_label.setObjectName("symbols_label")
        self.gridLayout_2.addWidget(self.symbols_label, 2, 0, 1, 1)
        self.num_label = QtWidgets.QLabel(self.gridLayoutWidget_2)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.num_label.setFont(font)
        self.num_label.setObjectName("num_label")
        self.gridLayout_2.addWidget(self.num_label, 0, 0, 1, 1)
        self.label = QtWidgets.QLabel(self.gridLayoutWidget_2)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.gridLayout_2.addWidget(self.label, 3, 0, 1, 1)
        self.numbers_text = QtWidgets.QLineEdit(self.custom_box)
        self.numbers_text.setGeometry(QtCore.QRect(95, 28, 61, 29))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.numbers_text.setFont(font)
        self.numbers_text.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.numbers_text.setStyleSheet("border: solid;\n"
"border-width: 1px;\n"
"border-color: rgb(0, 0, 0);")
        self.numbers_text.setInputMethodHints(QtCore.Qt.ImhDigitsOnly|QtCore.Qt.ImhFormattedNumbersOnly|QtCore.Qt.ImhPreferNumbers)
        self.numbers_text.setInputMask("")
        self.numbers_text.setMaxLength(32767)
        self.numbers_text.setCursorPosition(1)
        self.numbers_text.setObjectName("numbers_text")
        self.letters_text = QtWidgets.QLineEdit(self.custom_box)
        self.letters_text.setGeometry(QtCore.QRect(95, 81, 61, 29))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.letters_text.setFont(font)
        self.letters_text.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.letters_text.setStyleSheet("border: solid;\n"
"border-width: 1px;\n"
"border-color: rgb(0, 0, 0);")
        self.letters_text.setInputMethodHints(QtCore.Qt.ImhDigitsOnly|QtCore.Qt.ImhFormattedNumbersOnly|QtCore.Qt.ImhPreferNumbers)
        self.letters_text.setInputMask("")
        self.letters_text.setMaxLength(32767)
        self.letters_text.setEchoMode(QtWidgets.QLineEdit.Normal)
        self.letters_text.setCursorPosition(1)
        self.letters_text.setObjectName("letters_text")
        self.symbols_text = QtWidgets.QLineEdit(self.custom_box)
        self.symbols_text.setGeometry(QtCore.QRect(95, 133, 61, 29))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.symbols_text.setFont(font)
        self.symbols_text.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.symbols_text.setStyleSheet("border: solid;\n"
"border-width: 1px;\n"
"border-color: rgb(0, 0, 0);")
        self.symbols_text.setInputMethodHints(QtCore.Qt.ImhDigitsOnly|QtCore.Qt.ImhFormattedNumbersOnly|QtCore.Qt.ImhPreferNumbers)
        self.symbols_text.setInputMask("")
        self.symbols_text.setMaxLength(32767)
        self.symbols_text.setEchoMode(QtWidgets.QLineEdit.Normal)
        self.symbols_text.setCursorPosition(1)
        self.symbols_text.setObjectName("symbols_text")
        self.upletters_text = QtWidgets.QLineEdit(self.custom_box)
        self.upletters_text.setGeometry(QtCore.QRect(95, 182, 61, 29))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.upletters_text.setFont(font)
        self.upletters_text.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.upletters_text.setStyleSheet("border: solid;\n"
"border-width: 1px;\n"
"border-color: rgb(0, 0, 0);")
        self.upletters_text.setInputMethodHints(QtCore.Qt.ImhDigitsOnly|QtCore.Qt.ImhFormattedNumbersOnly|QtCore.Qt.ImhPreferNumbers)
        self.upletters_text.setInputMask("")
        self.upletters_text.setMaxLength(32767)
        self.upletters_text.setEchoMode(QtWidgets.QLineEdit.Normal)
        self.upletters_text.setCursorPosition(1)
        self.upletters_text.setObjectName("upletters_text")
        self.horizontalLayoutWidget = QtWidgets.QWidget(self.centralwidget)
        self.horizontalLayoutWidget.setGeometry(QtCore.QRect(30, 0, 461, 26))
        self.horizontalLayoutWidget.setObjectName("horizontalLayoutWidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setSpacing(40)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.random_radio = QtWidgets.QRadioButton(self.horizontalLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.random_radio.setFont(font)
        self.random_radio.setFocusPolicy(QtCore.Qt.NoFocus)
        self.random_radio.setStyleSheet("")
        self.random_radio.setObjectName("random_radio")
        self.horizontalLayout.addWidget(self.random_radio)
        self.custom_radio = QtWidgets.QRadioButton(self.horizontalLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.custom_radio.setFont(font)
        self.custom_radio.setFocusPolicy(QtCore.Qt.TabFocus)
        self.custom_radio.setObjectName("custom_radio")
        self.horizontalLayout.addWidget(self.custom_radio)
        self.result_box = QtWidgets.QGroupBox(self.centralwidget)
        self.result_box.setGeometry(QtCore.QRect(20, 260, 460, 41))
        self.result_box.setStyleSheet("background-color: #3f3f3f;\n"
"color: rgb(255, 255, 255);\n"
"border: display none;")
        self.result_box.setTitle("")
        self.result_box.setObjectName("result_box")
        self.result_label = QtWidgets.QLabel(self.result_box)
        self.result_label.setGeometry(QtCore.QRect(50, 10, 111, 21))
        font = QtGui.QFont()
        font.setPointSize(19)
        font.setBold(True)
        font.setItalic(True)
        font.setWeight(75)
        self.result_label.setFont(font)
        self.result_label.setStyleSheet("")
        self.result_label.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.result_label.setObjectName("result_label")
        self.pass_label = QtWidgets.QTextEdit(self.result_box)
        self.pass_label.setGeometry(QtCore.QRect(160, 4, 291, 31))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pass_label.sizePolicy().hasHeightForWidth())
        self.pass_label.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.pass_label.setFont(font)
        self.pass_label.setStyleSheet("border-style: solid;\n"
"border-width: 1px;\n"
"border-color: rgb(255, 255, 255);")
        self.pass_label.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.pass_label.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.pass_label.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.pass_label.setLineWrapMode(QtWidgets.QTextEdit.NoWrap)
        self.pass_label.setReadOnly(True)
        self.pass_label.setTabStopWidth(80)
        self.pass_label.setTextInteractionFlags(QtCore.Qt.TextSelectableByMouse)
        self.pass_label.setObjectName("pass_label")
        self.pass_status_text = QtWidgets.QLabel(self.centralwidget)
        self.pass_status_text.setGeometry(QtCore.QRect(80, 308, 341, 16))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.pass_status_text.setFont(font)
        self.pass_status_text.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.pass_status_text.setStyleSheet("color: rgb(0, 220, 7);")
        self.pass_status_text.setAlignment(QtCore.Qt.AlignCenter)
        self.pass_status_text.setObjectName("pass_status_text")
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Zaika - Password Generator"))
        self.from_label.setText(_translate("MainWindow", "From: "))
        self.to_label.setText(_translate("MainWindow", "To: "))
        self.from_text.setText(_translate("MainWindow", "16"))
        self.to_text.setText(_translate("MainWindow", "64"))
        self.letters_label.setText(_translate("MainWindow", "Letters:"))
        self.symbols_label.setText(_translate("MainWindow", "Symbols:"))
        self.num_label.setText(_translate("MainWindow", "Numbers:"))
        self.label.setText(_translate("MainWindow", "UpLetters:"))
        self.numbers_text.setText(_translate("MainWindow", "4"))
        self.letters_text.setText(_translate("MainWindow", "4"))
        self.symbols_text.setText(_translate("MainWindow", "4"))
        self.upletters_text.setText(_translate("MainWindow", "4"))
        self.random_radio.setText(_translate("MainWindow", "Random"))
        self.custom_radio.setText(_translate("MainWindow", "Custom"))
        self.result_label.setText(_translate("MainWindow", "Result:"))
        self.pass_status_text.setText(_translate("MainWindow", "PASSWORD COPIED IN CLIPBOARD"))
